import json
import os

current = os.path.dirname(os.path.realpath(__file__))
print("Current dir: " + current)

with open(current + '/issue_links.json') as f:
    data = json.load(f)

urls = {}
urls['data'] = []
for d in data:
    url = d['url'].rsplit('/', 2)[0] + '/all-cities/all-states/law-firms/'
    print(url)
    urls['data'].append({
        'url': url
    })

with open(current + '/first_firm_urls.json', 'w') as outfile:
    json.dump(urls, outfile, indent=4)
