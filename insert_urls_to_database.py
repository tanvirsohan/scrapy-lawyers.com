import mysql.connector
import json
import os

name = "urltodb"

current = os.path.dirname(os.path.realpath(__file__))

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="",
    database="dave_law"
)

mycursor = mydb.cursor()

with open(current + '/first_firm_urls.json') as url:
    urls = json.load(url)

for data in urls['data']:
    url = data['url']
    sql = "INSERT INTO lawyers_issue_url (url) VALUES ('" + url + "')"
    mycursor.execute(sql)
    mydb.commit()
    print(mycursor.rowcount, " record inserted.")

mycursor.close()
