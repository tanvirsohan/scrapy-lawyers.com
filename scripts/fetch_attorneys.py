import mysql.connector
import json
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

mydb = mysql.connector.connect(
    host = "localhost",
    user = "root",
    passwd = "tanvir",
    database = "dave_law"
)

mycursor = mydb.cursor()

def get_attorney_data(append_data):

    parent_finder = "section.fp-all-attorneys div.fp-attorneys-list"

    if append_data == True: append_function()

    button_available = check_next_page()

    if button_available is True:

        parent_class = driver.find_element_by_css_selector(parent_finder).get_attribute('class')
        parent_class_array = parent_class.split(' ')

        if 'ajax-loader' in parent_class_array:
            get_attorney_data(True)

        else:
            get_attorney_data(False)

    else: return


def append_function():
    list_finder = "section.fp-all-attorneys div.fp-attorneys-list div.show-for-medium-up ul.fp-attorney-item"

    list_item_finder = "section.fp-all-attorneys div.fp-attorneys-list div.show-for-medium-up ul.fp-attorney-item li div.fp-attorney-details a"

    WebDriverWait(driver, 10).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, list_finder)))

    list_items = driver.find_elements_by_css_selector(list_item_finder)

    for item in list_items:
        profile_url = item.get_attribute('href').replace('https:', '')
        attorneys.append(profile_url)


def check_next_page():
    try:
        li_finder = "section.fp-all-attorneys div.fp-attorneys-list div.show-for-medium-up div.pagination-area ul.pagination li"
        next_btn_finder = "section.fp-all-attorneys div.fp-attorneys-list div.show-for-medium-up div.pagination-area ul.pagination li button.pagination_next"

        li = WebDriverWait(driver, 10).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, li_finder)))

        last_li = li[len(li) - 1]
        last_li_class = last_li.get_attribute('class')

        if last_li_class == 'unavailable':
            return False
        else:
            next_btn = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, next_btn_finder)))
            next_btn.click()
            return True
    
    except:
        return False


select_sql = "SELECT url FROM firm_page_links WHERE is_fetched = '1' AND is_attorneys_fetched = '0' LIMIT 1"

mycursor.execute(select_sql)
myresult = mycursor.fetchone()

if myresult is not None:

    url = myresult[0]

    options = webdriver.FirefoxOptions()
    options.add_argument('-headless')

    driver = webdriver.Firefox(firefox_options=options)

    attorneys = []

    driver.get(url)

    get_attorney_data(True)

    driver.close()

    attorneys_json = json.dumps(attorneys)

    update_sql = "UPDATE firm_info SET attorney_list='{}' WHERE profile_url='{}'".format(attorneys_json, url)
    # print(update_sql)

    mycursor.execute(update_sql)
    mydb.commit()
    print("Table 'firm_info' updated:", mycursor.rowcount, "records(s) affected")

    another_update_sql = "UPDATE firm_page_links SET is_attorneys_fetched = '1' WHERE url = '{}'".format(url)

    mycursor.execute(another_update_sql)
    mydb.commit()
    print("Table 'firm_page_links' updated:", mycursor.rowcount, "records(s) affected")

else:
    print('No database row with empty attorneys list.')