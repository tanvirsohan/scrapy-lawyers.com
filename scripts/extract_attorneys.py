import mysql.connector
import datetime
import json


mydb = mysql.connector.connect(
    host = "localhost",
    user = "root",
    passwd = "tanvir",
    database = "dave_law"
)

mycursor = mydb.cursor()

select_sql = "SELECT id, attorney_list, is_sponsored, is_featured FROM firm_info WHERE is_attorney_extracted = '0'"

mycursor.execute(select_sql)
myresult = mycursor.fetchall()

if myresult is None:
    print('No database row left to extract.')
else:
    for res in myresult:
        id = res[0]
        attorney_list = res[1]
        is_sponsored = res[2]
        is_featured = res[3]

        attorneys = json.loads(attorney_list)

        for attorney in attorneys:

            sql = "SELECT id FROM attorney_profile_links WHERE url = '{}'".format(attorney)

            mycursor.execute(sql)
            result = mycursor.fetchall()

            if len(result) != 0:
                print('Skipped:', attorney)
                print('Already in database.')

            else:
                insert_sql = "INSERT INTO attorney_profile_links (firm_id, url, is_sponsored, is_featured) VALUES('{}', '{}', '{}', '{}')".format(id, attorney, is_sponsored, is_featured)

                mycursor.execute(insert_sql)
                mydb.commit()
                print(mycursor.rowcount, " record inserted.")



        current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        update_sql = "UPDATE firm_info SET is_attorney_extracted = '1', extraction_time = '{}' WHERE id = '{}'".format(current_time, id)
        
        mycursor.execute(update_sql)
        mydb.commit()
        print("Table 'firm_info' updated:", mycursor.rowcount, "records(s) affected")