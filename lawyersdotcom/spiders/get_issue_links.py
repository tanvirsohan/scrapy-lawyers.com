import scrapy
import os
import json
import mysql.connector


class LinksSpider(scrapy.Spider):
    name = "get_issue_links"
    start_urls = [
        'https://www.lawyers.com/find-a-lawyer/',
    ]

    current_dir = os.path.dirname(os.path.realpath(__file__))

    def parse(self, response):
        arr = []
        for link in response.css('div.content.abc-panel > ul.popular_items > li'):
            data = {
                'title': link.css('a::text').get(),
                'url': link.css('a').attrib['href'],
            }
            arr.append(data)

        with open(self.current_dir + "/../../issue_links.json", 'w') as json_file:
            json.dump(arr, json_file, indent=4)

        self.parseURL()

    def parseURL(self):
        with open(self.current_dir + '/../../issue_links.json') as f:
            data = json.load(f)

        urls = []
        for d in data:
            url = d['url'].rsplit('/', 2)[0] + \
                '/all-cities/all-states/law-firms/'
            # print(url)
            urls.append({
                'url': url
            })

        with open(self.current_dir + '/../../first_firm_urls.json', 'w') as outfile:
            json.dump(urls, outfile, indent=4)

        self.insertToDB()

    def insertToDB(self):
        mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            passwd="tanvir",
            database="dave_law"
        )

        mycursor = mydb.cursor()

        with open(self.current_dir + '/../../first_firm_urls.json') as url:
            urls = json.load(url)

        for data in urls:
            url = data['url']
            
            select_sql = "SELECT * FROM issue_url WHERE url = '{}'".format(url)
            mycursor.execute(select_sql)
            myresult = mycursor.fetchone()

            if myresult is None:
                sql = "INSERT INTO issue_url (url) VALUES ('" + url + "')"
                mycursor.execute(sql)
                mydb.commit()
                print(mycursor.rowcount, " record inserted.")
                print(url)
            else:
                print('\nSkipped: URL already in database')
                print(url)
                print('\n')

        mycursor.close()
