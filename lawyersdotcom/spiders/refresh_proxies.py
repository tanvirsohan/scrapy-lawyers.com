import scrapy
import os

from scrapy.spidermiddlewares.httperror import HttpError
from twisted.internet.error import DNSLookupError
from twisted.internet.error import TimeoutError, TCPTimedOutError


class RefreshProxies(scrapy.Spider):
    name = "refresh_proxies"
    current_dir = os.path.dirname(os.path.realpath(__file__))

    def start_requests(self):
        for u in self.start_urls:
            request = scrapy.Request(
                u, callback=self.parse, errback=self.errback, dont_filter=True)
            request.meta['proxy'] = None
            yield request

    def parse(self, response):
        proxies = []
        for data in response.css('tbody tr'):
            td = data.select('./td/text()').extract()
            host = td[0]
            port = td[1]
            https = td[6]

            if https == 'yes':
                url = 'https://' + host + ':' + port
            else:
                url = 'http://' + host + ':' + port

            proxies.append(url)

        text = ''
        for proxy in proxies:
            text += proxy + "\n"

        f = open(self.current_dir + "/../../proxy.txt", 'w')
        f.write(text)

    def errback(self, failure):
        # logs failures
        self.logger.error(repr(failure))

        if failure.check(HttpError):
            response = failure.value.response
            self.logger.error("HttpError occurred on %s", response.url)

        elif failure.check(DNSLookupError):
            request = failure.request
            self.logger.error("DNSLookupError occurred on %s", request.url)

        elif failure.check(TimeoutError, TCPTimedOutError):
            request = failure.request
            self.logger.error("TimeoutError occurred on %s", request.url)

    start_urls = [
        "https://free-proxy-list.net/"
    ]
