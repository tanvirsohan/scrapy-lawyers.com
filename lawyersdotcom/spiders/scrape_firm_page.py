import scrapy
import mysql.connector
import cfscrape
import datetime
import urllib.parse
import unicodedata
import re
import json
import phonenumbers
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="tanvir",
    database="dave_law"
)
mycursor = mydb.cursor()


def getStartURL():

    mycursor.execute(
        "SELECT id, url, is_sponsored, is_featured FROM firm_page_links WHERE is_fetched != '1' LIMIT 1")
    myresult = mycursor.fetchone()

    return_data = dict()

    if myresult is not None:
        return_data['url'] = myresult[1]
        return_data['is_sponsored'] = myresult[2]
        return_data['is_featured'] = myresult[3]

    return return_data


class ScrapeForProfile(scrapy.Spider):
    name = "scrape_firm_profile"

    return_data = getStartURL()

    start_urls = []

    if bool(return_data) is True:
        url = return_data['url']
        is_sponsored = return_data['is_sponsored']
        is_featured = return_data['is_featured']
        start_urls.append(url)

    def start_requests(self):
        for url in self.start_urls:
            token, agent = cfscrape.get_tokens(url)
            yield scrapy.Request(url=url, cookies=token, headers={'User-Agent': agent})

    def parse(self, response):
        # Profile URL
        profile_url = response.url

        item = response.css('section.profile-summary-area')

        # Name
        firmname = item.css(
            'h1.profile-summary-title span::text').get().strip().replace("'","\\'")

        # Website
        website = item.css(
            'div.profile-summary-contact p span.psc-website a').attrib['href'].strip()

        # Phone
        phone = item.css(
            'div.pbl-btn-phone-website ul li div.pbl-phone a::text').get().strip()

        if phone == 'View Phone #':
            phone = item.css(
                'div.pbl-btn-phone-website ul li div.pbl-phone a').attrib['data-phonenum'].strip()

        location_string = item.css(
            'div.profile-floating-contact div.pbl-inner p.pbl-serving-location span.span-block-box b::text').get()
        location_string = location_string.split(',')

        # City
        city = location_string[0].strip().replace("'","\\'")

        # State
        state = location_string[1].strip().replace("'","\\'")

        review_item = item.css(
            'div.profile-summary-reviews ul li div.break-space big strong::text').getall()
        
        # Peer rating
        try:
            peer_rating = review_item[0]
        except:
            peer_rating = 'n/a'

        # Client rating
        try:
            client_rating = review_item[1]
        except:
            client_rating = 'n/a'

        awards = item.css(
            'div.profile-summary-awards.hide-for-small-only div.left img::attr(alt)').getall()

        # Awards
        # for i, award in enumerate(awards):
        #     award = award.replace("'","\\'")
        #     awards[i] = award

        awards_json = json.dumps(awards)
        awards_json = awards_json.replace("'","\\'")

        item = response.css('section.profile-area-of-law')

        areas_of_law = item.css(
            'div.profile-aops-area ul li span::text').getall()
        
        # Areas of law
        # for i, area in enumerate(areas_of_law):
        #     areas_of_law[i] = area

        areas_of_law_json = json.dumps(areas_of_law)
        areas_of_law_json = areas_of_law_json.replace("'","\\'")

        item = response.css('div.profile-contact-information')

        address_arr = item.css('div.pc-address span a span::text').getall()

        for i in range(len(address_arr)):
            address_arr[i] = unicodedata.normalize(
                'NFKD', address_arr[i].strip())

        length = len(address_arr)
        
        address_country = address_arr[length - 1]
        address_zip = address_arr[length - 2]
        address_state = address_arr[length - 3]
        address_city = address_arr[length - 4]
        address_location = ''
        for i in range(length - 4):
            address_location += address_arr[i]

        print('Country:', address_country)
        print('ZIP:', address_zip)
        print('State:', address_state)
        print('City:', address_city)
        print('Address:', address_location)

        # ZIP
        zip = address_zip.replace("'","\\'")
        
        # Address
        address = "{}, {}{} {}, {}".format(
            address_location, address_city, address_state, address_zip, address_country).replace("'","\\'")
        item = response.css('section#reviews-section')

        # Peer review
        peer_review = item.css(
            'article ul.tabs li.tab-title a[data-id=PeerReviews]::text').get().split(' ')[1]
        try:
            peer_review = re.findall('[0-9]+', peer_review)[0]
        except:
            peer_review = ''
        
        # Client review
        client_review = item.css(
            'article ul.tabs li.tab-title a[data-id=ClientReviews]::text').get().split(' ')[1]
        try:
            client_review = re.findall('[0-9]+', client_review)[0]
        except:
            client_review = ''

        # Getting attorney list
        item = response.css('section.fp-all-attorneys div.fp-attorneys-list div.show-for-medium-up')

        pagination = item.css('div.pagination-area.show-for-medium-up').getall()
        pagination_count = len(pagination)

        if pagination_count == 0:
            item = response.css('section.fp-all-attorneys div.fp-attorneys-list div.show-for-medium-up ul.fp-attorney-item')
            
            attorney_list = item.css('li div.fp-attorney-photo a::attr(href)').getall()
            
            # Attorneys
            attorneys_json = json.dumps(attorney_list)
            is_lawyers_fetched = True

        else: 
            attorneys_json = ""
            is_lawyers_fetched = False

        data = {
            'name': firmname,
            'profile_url': profile_url,
            'website': website,
            'phone': phone,
            'city': city,
            'state': state,
            'peer_rating': peer_rating,
            'client_rating': client_rating,
            'awards': awards_json,
            'areas_of_law': areas_of_law_json,
            'zip': zip,
            'address': address,
            'peer_review': peer_review,
            'client_review': client_review,
            'is_lawyers_fetched': is_lawyers_fetched,
            'attorneys': attorneys_json,
            'is_sponsored': self.is_sponsored,
            'is_featured': self.is_featured
        }

        databaseInsert(data)


def databaseInsert(data):
    select_sql = "SELECT id FROM firm_info WHERE profile_url = '{}'".format(
        data['profile_url'])

    mycursor.execute(select_sql)
    myresult = mycursor.fetchone()

    if myresult is None:
        
        phone = phonenumbers.parse(data['phone'], "US")
        format_phone = phonenumbers.format_number(phone, phonenumbers.PhoneNumberFormat.INTERNATIONAL)
        data['phone'] = format_phone.replace('-', '').replace('(', '').replace(')', '').replace(' ', '')

        insert_sql = "INSERT INTO firm_info (name, profile_url, website, phone, city, state, address, zip, areas_of_law, attorney_list, peer_rating, peer_review, client_rating, client_review, awards, is_sponsored, is_featured) VALUES('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', {}, {})".format(data['name'], data['profile_url'], data['website'], data['phone'], data['city'], data['state'], data['address'], data['zip'], data['areas_of_law'], data['attorneys'], data['peer_rating'], data['peer_review'], data['client_rating'], data['client_review'], data['awards'], data['is_sponsored'], data['is_featured'])
        
        mycursor.execute(insert_sql)
        mydb.commit()
        print("Inserted:", data['profile_url'])

    else:
        print('Skipped:', data['profile_url'])
        print('Already in database')

    # UPDATE DATABASE WITH is_fetched AND is_attorneys_fetched
    is_attorneys_fetched = 1 if data['is_lawyers_fetched'] is True else 0
    current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    
    update_sql = "UPDATE firm_page_links SET is_fetched='1', fetched_at='{}', is_attorneys_fetched='{}' WHERE url='{}'".format(current_time, is_attorneys_fetched, data['profile_url'])

    mycursor.execute(update_sql)
    mydb.commit()
    print(mycursor.rowcount, "records(s) affected")
