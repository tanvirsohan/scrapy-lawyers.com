import scrapy
import mysql.connector
import os
import datetime


class GetFirmPageURLs(scrapy.Spider):
    name = "firmpageurl"

    current_dir = os.path.dirname(os.path.realpath(__file__))

    current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="",
        database="dave_law"
    )

    mycursor = mydb.cursor()

    mycursor.execute("SELECT * FROM lawyers_issue_url LIMIT 1")

    myresult = mycursor.fetchall()

    for x in myresult:
        print(x)

    mycursor.close()
