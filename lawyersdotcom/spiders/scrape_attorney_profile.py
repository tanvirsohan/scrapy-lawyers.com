import scrapy
import mysql.connector
import cfscrape
import datetime
import phonenumbers
import json
import re
from bs4 import BeautifulSoup

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="tanvir",
    database="dave_law"
)

mycursor = mydb.cursor()

def getStartURL():

    mycursor.execute("SELECT id, url, firm_id, is_sponsored, is_featured FROM attorney_profile_links WHERE is_fetched != '1' ORDER BY `id` ASC LIMIT 1")

    myresult = mycursor.fetchone()

    return_data = dict()

    if myresult is not None:
        return_data['url'] = myresult[1]
        return_data['firm_id'] = myresult[2]
        return_data['is_sponsored'] = myresult[3]
        return_data['is_featured'] = myresult[4]

    return return_data

class ScrapeForProfile(scrapy.Spider):

    name = "scrape_attorney"

    return_data = getStartURL()
    start_urls = []

    if bool(return_data) is True:
        url = return_data['url']
        firm_id = return_data['firm_id']
        is_sponsored = return_data['is_sponsored']
        is_featured = return_data['is_featured']

        start_urls.append('https:' + url)

    def start_requests(self):
        for url in self.start_urls:
            token, agent = cfscrape.get_tokens(url)
            yield scrapy.Request(url=url, cookies=token, headers={'User-Agent': agent})

    def parse(self, response):
        print('URL:', response.url)

        dburl = response.url.replace('https:', '')

        item = response.css("section.profile-summary-area")

        # name
        name = item.css('h1.profile-summary-title::text').get()
        # print('Name:', name)
    
        ratings = item.css('div.profile-summary-reviews ul li div.break-space big strong::text').getall()
        
        # peer rating
        try:
            peer_rating = ratings[0]
        except:
            peer_rating = 'n/a'
        # print('Peer rating:', peer_rating)

        # client rating
        try:
            client_rating = ratings[1]
        except:
            client_rating = 'n/a'
        # print('Client rating:', client_rating)

        # phone
        phone = item.css('div.profile-floating-contact ul li.sticky-phone a.fb-p-phone::attr(href)').get()

        if phone == 'javascript:;':
            phone = item.css('div.profile-floating-contact ul li.sticky-phone a.fb-p-phone::attr(data-phonenum)').get()
        # print(phone)
        phone = phone.replace('tel:', '')
        phone = phonenumbers.parse(phone, "US")
        phone = phonenumbers.format_number(phone, phonenumbers.PhoneNumberFormat.INTERNATIONAL)
        phone = phone.replace('-', '').replace('(', '').replace(')', '').replace(' ', '')

        # print('Phone:', phone)

        # website
        website = item.css('div.profile-floating-contact ul li.sticky-website a.fb-p-website::attr(href)').get()
        
        # print('Website:', website)

        location = item.css('div.more-details p.pbl-serving-location span b::text').get().split(',')
        
        # city
        city = location[0].strip().replace("'","\\'")
        
        # print('City:', city)

        # state
        state = location[1].strip().replace("'","\\'")

        # print('State:', state)

        # image
        try:
            image = item.css('img.ap-attorney-photo::attr(data-echo)').get()
        except:
            image = ''

        # print('Image:', image)

        # awards
        awards = item.css('div.peer-award img::attr(alt)').getall()

        awards_json = json.dumps(awards)
        awards_json = awards_json.replace("'", "\\'")
        
        # print('Awards:', awards_json)
        
        # license
        try:
            licensed = item.css('div.more-details div.show-for-medium-up p::text').get()
            licensed = re.findall('[0-9]+', licensed)[0]
        except:
            licensed = 'n/a'

        # print('Licensed for:', licensed)

        # areas of law
        item = response.css('section.profile-area-of-law')

        area_of_law = item.css('div.profile-aops-area ul li span::text').getall()
 
        area_of_law_json = json.dumps(area_of_law)
        area_of_law_json = area_of_law_json.replace("'","\\'")
        # print('Area of law:', area_of_law)

        item = response.css('div.profile-contact-information div.loc-section.pc-address span')
        
        # address
        streetAddress = item.css('span[itemprop=streetAddress]::text').get()
        addressLocality = item.css('span[itemprop=addressLocality]::text').get()
        postalCode = item.css('span[itemprop=postalCode]::text').get()
        addressRegion = item.css('span[itemprop=addressRegion]::text').get()

        address_string = "{}, {}, {} {}, {}".format(streetAddress, addressLocality, state, postalCode, addressRegion)
        # print('Address:', address_string)
        address_string = address_string.replace("'", "\\'")

        # zip
        zip = postalCode
        # print('Zip:', zip)
        
        item = response.css('section.reviews.ap-reviews')
        tabs = item.css('ul.tabs li a::text').getall()
        
        # client review
        try:
            client_review = re.findall('[0-9]+', tabs[0])[0]
        except:
            client_review = 'n/a'

        # print('Client review:', client_review)
        
        # peer review
        try:
            peer_review = re.findall('[0-9]+', tabs[1])[0]
        except:
            peer_review = 'n/a'

        # print('Peer review:', peer_review)


        item = response.css('section.profile-details.ap-credentials')  
        details = item.css('div.profile-detail-item')
        
        subject = details.css('div.profile-sub-title-area strong::text').getall()
        content = details.css('div.profile-credentials-content-area div.truncate-text').getall()
        
        # position
        try:
            pos_index = subject.index('Position')
            soup = BeautifulSoup(content[pos_index], features="lxml")
            position = soup.get_text(", ", strip=True)
        except:
            position: 'n/a'

        position = position.replace("'", "\\'")

        # print('Position:', position)
        
        # education
        try:
            uni_index = subject.index('University Attended')
            soup = BeautifulSoup(content[uni_index], features="lxml")
        
            university = soup.get_text(", ", strip=True)
        except:
            university = ''
    
        try:
            law_index = subject.index('Law School Attended')
            soup = BeautifulSoup(content[law_index], features="lxml")
    
            lawschool = soup.get_text(", ", strip=True)
        except:
            lawschool = ''

        if len(university) == 0 and len(lawschool) == 0: education = ''
        elif len(university) == 0 and len(lawschool) != 0: education = lawschool
        elif len(university) != 0 and len(lawschool) == 0: education = university
        else: education = university + ", " + lawschool

        education = education.replace("'","\\'")
        
        # print('Education:', education)

        # birth information
        try:
            birth_index = subject.index('Birth Information')
            soup = BeautifulSoup(content[birth_index], features="lxml")

            birth_info = soup.get_text(", ", strip=True)
        except:
            birth_info = 'n/a'

        birth_info = birth_info.replace("'", "\\'")

        # print('Birth info:', birth_info)

        # membership
        try:
            member_index = subject.index('Associations & Memberships')
            soup = BeautifulSoup(content[member_index], features="lxml")

            membership = soup.get_text("; ", strip=True)

        except:
            membership = 'n/a'

        membership = membership.replace("'", "\\'")

        # print('Membership:', membership)

        # certification
        try:
            certification_index = subject.index('Certifications')
            soup = BeautifulSoup(content[certification_index], features="lxml")

            certification_info = soup.get_text(", ", strip=True)
        except:
            certification_info = "n/a"

        certification_info = certification_info.replace("'", "\\'")

        # print('Certification:', certification_info)

        # Inserting in db
        select_sql = "SELECT id FROM attorney_info WHERE profile_url='{}'".format(response.url)

        mycursor.execute(select_sql)
        myresult = mycursor.fetchone()

        if myresult is None:
            insert_sql = "INSERT INTO attorney_info (profile_url, firm_id, name, phone, website, city, state, image, peer_rating, client_rating, awards, license, areas_of_law, address, zip, client_review, peer_review, position, education, birth_info, membership, certification, is_featured, is_sponsored) VALUES('{}', {}, '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', {}, {})".format(response.url, self.firm_id, name, phone, website, city, state, image, peer_rating, client_rating, awards_json, licensed, area_of_law_json, address_string, zip, client_review, peer_review, position, education, birth_info, membership, certification_info, self.is_featured, self.is_sponsored)

            print(insert_sql)

            mycursor.execute(insert_sql)
            mydb.commit()
            print('Inserted:', response.url)
        else:
            print('Skipped:', response.url)
            print('Already in database')

        # UPDATE DATABASE
        current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        update_sql = "UPDATE attorney_profile_links SET is_fetched='1', fetched_at='{}' WHERE url='{}'".format(current_time, dburl)

        mycursor.execute(update_sql)
        mydb.commit()
        print(mycursor.rowcount, "record(s) affected")