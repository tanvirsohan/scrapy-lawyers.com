import scrapy
import mysql.connector
import cfscrape
import datetime
import urllib.parse

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="tanvir",
    database="dave_law"
)
mycursor = mydb.cursor()


def getStartURL():

    mycursor.execute(
        "SELECT * FROM issue_url WHERE is_fetched != '1' LIMIT 1")
    myresult = mycursor.fetchone()

    if myresult is not None:
        result = myresult[1]

    else:
        result = None

    return_data = []
    return_data.append(result)

    return return_data


def insertLink(url, is_sponsored, is_featured):
    sponsored = 1 if is_sponsored is True else 0
    featured = 1 if is_featured is True else 0
    url = urllib.parse.quote(url, safe="/:")
    select_sql = "SELECT id FROM firm_page_links WHERE url = '{}'".format(
        url)
    mycursor.execute(select_sql)
    myresult = mycursor.fetchone()

    if myresult is None:
        insert_sql = "INSERT INTO firm_page_links (url, is_sponsored, is_featured) VALUES('{}', {}, {})".format(
            url, sponsored, featured)
        mycursor.execute(insert_sql)
        mydb.commit()
        print("Inserted:", url)

    else:
        print('Skipped:', url)


def updateTable(url):
    url = urllib.parse.quote(url, safe="/:")
    current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    update_sql = "UPDATE issue_url SET is_fetched='1', fetched_at='{}' WHERE url='{}'".format(
        current_time, url)

    mycursor.execute(update_sql)
    mydb.commit()
    print(mycursor.rowcount, "records(s) affected")


class ScrapeForProfile(scrapy.Spider):
    name = "get_profile_links"

    start_urls = getStartURL()

    def start_requests(self):
        for url in self.start_urls:
            token, agent = cfscrape.get_tokens(url)
            yield scrapy.Request(url=url, cookies=token, headers={'User-Agent': agent})

    def parse(self, response):
        print('Current URL:', response.url)
        for item in response.css('div.search-results-list'):
            print('-------------------------')

            classes = item.xpath("@class").extract()[0]

            arr = classes.split(' ')

            if 'sponsor-result' in arr:
                is_sponsored = True
            else:
                is_sponsored = False

            if 'subscriber-result' in arr:
                is_featured = True
            else:
                is_featured = False

            link_url = item.css(
                'a.webstats-sponsorship-info.opt-d-title').attrib['href']

            link_url = 'https:' + link_url

            insertLink(link_url, is_sponsored, is_featured)

        pagination = response.css(
            'ul.pagination')
        link_array = pagination.css('li a.prevnext::attr(href)').extract()

        link = link_array[1]
        length = len(link)

        if length != 0:
            next_page = 'https://www.lawyers.com' + link
            token, agent = cfscrape.get_tokens(next_page)
            request = scrapy.Request(url=next_page, cookies=token, headers={
                                     'User-Agent': agent})
            yield request

        else:
            first_link = self.start_urls[0]
            updateTable(first_link)
